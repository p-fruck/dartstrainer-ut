# DartsTrainer UT wrapper

DartsTrainer is a Web based Darts training app. This PWA will be opened in the Ubuntu WebView.
Learn more about DartsTrainer on [dartstrainer.eu](https://dartstrainer.eu).

<a href="https://open-store.io/app/eu.dartstrainer.app.clickable">
    <img width="200" height="80" src="https://open-store.io/badges/en_US.svg" alt="OpenStore" />
</a>

## License

Copyright (C) 2021  p-fruck

Licensed under the Apache Software License 2.0
